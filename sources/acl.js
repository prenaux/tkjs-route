//
// ACL (Account/Access Control List)
//
var NI = require('tkjs-ni');

var UTILS = require('./utils');
exports.checkUserACL = UTILS.checkUserACL;
exports.checkIsUser = UTILS.checkIsUser;

var _aclRedirectToAuthCallback;
function aclSetRedirectToAuthCallback(aCallback) {
  _aclRedirectToAuthCallback = aCallback;
}
exports.aclSetRedirectToAuthCallback = aclSetRedirectToAuthCallback;

function aclRedirectToAuthLogin(aclCheck) {
  var url = aclCheck.req && aclCheck.req.url;
  if (NI.stringStartsWith(url, "/api/")) {
    aclCheck.res.status(403).send({ error: "forbidden access to " + url });
  }
  else if (_aclRedirectToAuthCallback) {
    aclCheck.onRedirect(
      aclCheck,
      _aclRedirectToAuthCallback(url, aclCheck));
  }
  else if (NI.stringStartsWith(url, "/mob/")) {
    aclCheck.onRedirect(aclCheck, "/mob/auth/login");
  }
  else {
    aclCheck.onRedirect(aclCheck, "/auth/login");
  }
}
exports.aclRedirectToAuthLogin = aclRedirectToAuthLogin;

function aclLoginCheck(aclCheck) {
  if (!aclCheck.session ||
      !aclCheck.session.user_acl ||
      NI.isEmpty(aclCheck.session.user_name))
  {
    if (aclCheck.onRedirect) {
      aclRedirectToAuthLogin(aclCheck);
    }
    else {
      aclCheck.onForbbiden(aclCheck,"Access denied: " + aclCheck.route.name);
    }
    return false;
  }
  return true;
}
exports.aclLoginCheck = aclLoginCheck;

var _canUse = {
  locked: function(aclCheck) {
    aclCheck.onForbbiden(aclCheck,"Locked: " + aclCheck.route.name);
  },

  public: function(aclCheck) {
    aclCheck.onGranted(aclCheck);
  },

  mustBeAuthed: function(aclCheck) {
    if (!aclLoginCheck(aclCheck)) {
      return;
    }
    aclCheck.onGranted(aclCheck);
  },

  userACL: function(aACLRequired) {
    return function(aclCheck) {
      if (!aclLoginCheck(aclCheck)) {
        return;
      }
      if (UTILS.checkUserACL(aclCheck.session.user_acl, aACLRequired)) {
        aclCheck.onGranted(aclCheck);
      }
      else {
        aclCheck.onForbbiden(aclCheck,"Access denied: " + aclCheck.route.name);
      }
    }
  }
}
exports.canUse = _canUse;

var _aclExactRoute = {};
var _aclStartsWithRoute = [];

function validateACL(aObject) {
  NI.assert.has(aObject,"name");
  NI.assert.isFunction(aObject["canUse"]);
  return aObject;
}
exports.validateACL = validateACL;

function addRouteExact(aObject, aDontLog) {
  if (NI.isArray(aObject)) {
    NI.forEach(aObject,function(acl) {
      addRouteExact(acl);
    });
  }
  else {
    validateACL(aObject);
    if (!aDontLog) {
      NI.info("ACL: exact: %s", aObject.name);
    }
    _aclExactRoute[aObject.name] = aObject;
  }
}
exports.addRouteExact = addRouteExact;

function addRouteStartsWith(aObject, aDontLog) {
  if (NI.isArray(aObject)) {
    NI.forEach(aObject,function(acl) {
      addRouteStartsWith(acl);
    });
  }
  else {
    validateACL(aObject);
    if (!aDontLog) {
      NI.info("ACL: startsWith: %s", aObject.name);
    }
    _aclStartsWithRoute.push(aObject);
  }
}
exports.addRouteStartsWith = addRouteStartsWith;

function getCanUseACL(aURL, aACL) {
  var canUseFunc;
  if (aACL === true) {
    aACL = "<<auth>>";
    canUseFunc = _canUse.mustBeAuthed;
  }
  else if ((typeof aACL === "string") &&
           (NI.stringStartsWith(aACL,"_") ||
            NI.stringEndsWith(aACL,"_")))
  {
    if (aACL === "__locked__") {
      canUseFunc = _canUse.locked;
    }
    else if (aACL === "__public__") {
      canUseFunc = _canUse.public;
    }
    else if (aACL === "__superadmin__") {
      canUseFunc = _canUse.userACL(aACL);
    }
    else {
      throw NI.Error("Invalid ACL definition for '%s': %k, unknown reserved ACL.",
                     aURL, aACL)
    }
  }
  else if (NI.isArray(aACL) || (typeof aACL === "string")) {
    canUseFunc = _canUse.userACL(aACL)
  }
  else {
    throw NI.Error("Invalid ACL definition for '%s': %k", aURL, aACL)
  }
  return {
    func: canUseFunc,
    desc: aACL
  };
}
exports.getCanUseACL = getCanUseACL;

// High level registration of an ACL
// aRoute: if ends with "/" addRouteStartsWith, otherwise addRouteExact(url) & addRouteExact(url+"/").
function registerACL(aURL, aACL) {
  if (NI.isArray(aURL)) {
    NI.forEach(aURL,function(acl) {
      registerACL(acl.url, acl.acl);
    });
    return;
  }
  NI.assert.isString(aURL);

  if (NI.stringContains(aURL,"?")) {
    throw NI.Error("ACL for '%s' cannot contain a '?', that is reserved for query parameters.", aURL);
  }

  var foundACL = findACL(aURL);
  if (foundACL) {
    if (aACL == "__existing__") {
      return;
    }
    throw NI.Error("An ACL is already defined for '%s': %k", aURL, foundACL);
  }
  else if (aACL == "__existing__") {
    throw NI.Error("An ACL is expected to be already declared for '%s', but none was found.", aURL);
  }

  var canUseACL = getCanUseACL(aURL, aACL);
  if (NI.stringEndsWith(aURL,"/")) {
    NI.info("ACL: startsWith: %s: %k", aURL, canUseACL.desc);
    addRouteStartsWith({
      name: aURL,
      canUse: canUseACL.func
    }, true);
  }
  else {
    NI.info("ACL: exact: %s: %k", aURL, canUseACL.desc);
    addRouteExact({
      name: aURL,
      canUse: canUseACL.func
    }, true);
    var urlWithSlash = aURL + "/";
    if (!findACL(urlWithSlash)) {
      addRouteExact({
        name: urlWithSlash,
        canUse: canUseACL.func
      }, true);
    }
  }
}
exports.registerACL = registerACL;

function findACL(url) {
  var aclRoute;

  // route has to be checked ignoring the query parameters
  var queryParamIndex = url.indexOf("?");
  if (queryParamIndex >= 0) {
    url = url.substring(0, queryParamIndex);
  }

  if (url in _aclExactRoute) {
    return _aclExactRoute[url];
  }
  else {
    for (var i = _aclStartsWithRoute.length-1; i >= 0; --i) {
      aclRoute = _aclStartsWithRoute[i];
      if (NI.stringStartsWith(url, aclRoute.name)) {
        break;
      }
      aclRoute = undefined;
    }
  }
  if (!aclRoute || aclRoute.canUse === undefined) {
    return undefined;
  }
  return aclRoute;
}
exports.findACL = findACL;

function checkACL(aclCheck) {
  NI.assert.has(aclCheck,"route");
  NI.assert.has(aclCheck,"session");
  NI.assert.has(aclCheck,"onGranted");
  NI.assert.has(aclCheck,"onForbbiden");

  var canUse = aclCheck.route.canUse;
  if (typeof canUse === "function") {
    // canUse handler will take care of the ACL dispatching...
    canUse(aclCheck);
  }
  else {
    // Invalid "canUse" type so... nope
    aclCheck.onForbbiden(aclCheck, "Invalid ACL definition, canUse not a function! " + aclCheck.route.name);
  }
}
exports.checkACL = checkACL;

function init() {
  addRouteExact([
    { name: "/", canUse: _canUse.public }
  ]);
  registerACL([
    { url: "/favicon.ico", acl: "__public__" },
    { url: "/favicon.png", acl: "__public__" },
    { url: "/apple-touch-icon.png", acl: "__public__" },
    { url: "/robots.txt", acl: "__public__" },
    { url: "/sitemap.xml", acl: "__public__" },
    { url: "/build/", acl: "__public__" },
    { url: "/js/", acl: "__public__" },
    { url: "/css/", acl: "__public__" },
    { url: "/fonts/", acl: "__public__" },
    { url: "/img/", acl: "__public__" },
  ]);
}
exports.init = init;
