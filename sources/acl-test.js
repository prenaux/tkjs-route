/* global FIXTURE, TEST */
var NI = require('tkjs-ni');
var ACL = require('./acl');

FIXTURE("ACL", function() {

  TEST("checkUserACL_SuperAdmin", function() {
    var superAdminACL = { __superadmin__: true };
    NI.assert.isTrue(ACL.checkUserACL(superAdminACL, "foo"));
    NI.assert.isTrue(ACL.checkUserACL(superAdminACL, "bar"));
    NI.assert.isTrue(ACL.checkUserACL(superAdminACL, "narf"));
    NI.assert.isTrue(ACL.checkUserACL(superAdminACL, "967CF003-75C0-432D-BFF5-307CA6AB382F"));
    NI.assert.isTrue(ACL.checkUserACL(superAdminACL, ["foo","bar"]));
    NI.assert.isTrue(ACL.checkUserACL(superAdminACL, ["foo","bar","narf"]));
  })

  TEST("checkUserACL_Test", function() {
    var testUserACL = { foo: true, bar: true };
    NI.assert.isTrue(ACL.checkUserACL(testUserACL, "foo"));
    NI.assert.isTrue(ACL.checkUserACL(testUserACL, "bar"));
    NI.assert.isTrue(!ACL.checkUserACL(testUserACL, "narf"));
    NI.assert.isTrue(!ACL.checkUserACL(testUserACL, "967CF003-75C0-432D-BFF5-307CA6AB382F"));
    NI.assert.isTrue(ACL.checkUserACL(testUserACL, ["foo","bar"]));
    NI.assert.isTrue(!ACL.checkUserACL(testUserACL, ["foo","bar","narf"]));
  })

  TEST("checkUserACL false values", function() {
    var testUserACL = { foo: true, bar: true, baz: false };
    NI.assert.isTrue(ACL.checkUserACL(testUserACL, "foo"));
    NI.assert.isTrue(ACL.checkUserACL(testUserACL, "bar"));
    NI.assert.isTrue(!ACL.checkUserACL(testUserACL, "baz"));
    NI.assert.isTrue(!ACL.checkUserACL(testUserACL, "narf"));
    NI.assert.isTrue(!ACL.checkUserACL(testUserACL, "967CF003-75C0-432D-BFF5-307CA6AB382F"));

    NI.assert.isTrue(ACL.checkUserACL(testUserACL, ["foo","bar"]));
    NI.assert.isTrue(ACL.checkUserACL(testUserACL, ["OR","baz","foo"]));
    NI.assert.isTrue(!ACL.checkUserACL(testUserACL, ["AND","baz","foo"]));
    NI.assert.isTrue(!ACL.checkUserACL(testUserACL, ["foo","bar","narf"]));
  })

  TEST("routes", function() {
    var didThrow;
    ACL.registerACL("/downloads",["__public__"]);

    didThrow = false;
    try {
      ACL.registerACL("/downloads",["__public__"]);
    } catch(e) { didThrow = true; }
    NI.assert.isTrue(didThrow);

    didThrow = false;
    try {
      ACL.registerACL("/another-route?",["__public__"]);
    } catch(e) {
      // throws with "? cannot be used in route"
      didThrow = true;
    }
    NI.assert.isTrue(didThrow);

    NI.assert.isTrue(ACL.findACL("/downloads") != null);
    NI.assert.isTrue(ACL.findACL("/downloads/") != null);
    NI.assert.isTrue(ACL.findACL("/downloads?foo=123") != null);
    NI.assert.isTrue(ACL.findACL("/downloads/?foo=123") != null);
    NI.assert.isFalse(ACL.findACL("/downloads/foo") != null);

    ACL.registerACL("/somecategory/",["__public__"]);

    didThrow = false;
    try {
      ACL.registerACL("/somecategory/stuff",["__public__"]);
    } catch(e) {
      didThrow = true;
    }
    NI.assert.isTrue(didThrow);

    NI.assert.isTrue(ACL.findACL("/somecategory/") != null);
    NI.assert.isTrue(ACL.findACL("/somecategory/foo/bar") != null);
    NI.assert.isTrue(ACL.findACL("/somecategory/all/the/stuff") != null);
    NI.assert.isFalse(ACL.findACL("/somecategory") != null);

    ACL.registerACL("/somecategory",["__public__"]);
    NI.assert.isTrue(ACL.findACL("/somecategory") != null);

    // Order matters to register both routes. "/admin" will register both
    // "/admin" and "/admin/" if "/admin/" hasn't been registered before, that
    // is because they are generally equivalent and users expects them to be.
    ACL.registerACL("/admin/",["__public__"]);
    ACL.registerACL("/admin",["__public__"]);
    NI.assert.isTrue(ACL.findACL("/somecategory") != null);
    NI.assert.isTrue(ACL.findACL("/somecategory/") != null);
    NI.assert.isTrue(ACL.findACL("/somecategory/foo/bar") != null);
  });
})
