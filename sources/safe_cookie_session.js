// see https://github.com/mozilla/node-client-sessions
var NI = require('tkjs-ni');
var Cookies = require("cookies");
var ACTIVE_DURATION = 1000 * 60 * 5;
var SAFE_COOKIE = require("./safe_cookie");

/* map from hmac algorithm to _minimum_ key byte length */
var SIGNATURE_ALGORITHMS = {
  'sha256': 32,
  'sha256-drop128': 32,
  'sha384': 48,
  'sha384-drop192': 48,
  'sha512': 64,
  'sha512-drop256': 64
};

/* map from cipher algorithm to exact key byte length */
var ENCRYPTION_ALGORITHMS = {
  aes128: 16, // implicit CBC mode
  aes192: 24,
  aes256: 32
};

function keyConstraints(opts) {
  if (!Buffer.isBuffer(opts.encryptionKey)) {
    throw new Error('encryptionKey must be a Buffer');
  }
  if (!Buffer.isBuffer(opts.signatureKey)) {
    throw new Error('signatureKey must be a Buffer');
  }

  if (SAFE_COOKIE.constantTimeEquals(opts.encryptionKey, opts.signatureKey)) {
    throw new Error('Encryption and Signature keys must be different');
  }

  var encAlgo = opts.encryptionAlgorithm;
  var required = ENCRYPTION_ALGORITHMS[encAlgo];
  if (opts.encryptionKey.length !== required) {
    throw new Error(
      'Encryption Key for '+encAlgo+' must be exactly '+required+' bytes '+
        '('+(required*8)+' bits)'
    );
  }

  var sigAlgo = opts.signatureAlgorithm;
  var minimum = SIGNATURE_ALGORITHMS[sigAlgo];
  if (opts.signatureKey.length < minimum) {
    throw new Error(
      'Encryption Key for '+sigAlgo+' must be at least '+minimum+' bytes '+
        '('+(minimum*8)+' bits)'
    );
  }
}

/*
 * Session object
 *
 * this should be implemented with proxies at some point
 */
function Session(req, res, cookies, opts) {
  this.req = req;
  this.res = res;
  this.cookies = cookies;
  this.opts = opts;
  if (opts.cookie.ephemeral && opts.cookie.maxAge) {
    throw new Error("you cannot have an ephemeral cookie with a maxAge.");
  }

  this.content = {};
  this.json = JSON.stringify(this._content);
  this.loaded = false;
  this.dirty = false;

  // no need to initialize it, loadFromCookie will do
  // via reset() or unbox()
  this.createdAt = null;
  this.duration = opts.duration;
  this.activeDuration = opts.activeDuration;

  // support for maxAge
  if (opts.cookie.maxAge) {
    this.expires = new Date(new Date().getTime() + opts.cookie.maxAge);
  } else {
    this.updateDefaultExpires();
  }

  // here, we check that the security bits are set correctly
  var secure = (res.socket && res.socket.encrypted) ||
      (req.connection && req.connection.proxySecure);
  if (opts.cookie.secure && !secure) {
    throw new Error("you cannot have a secure cookie unless the socket is " +
        " secure or you declare req.connection.proxySecure to be true.");
  }
}

Session.prototype = {
  updateDefaultExpires: function() {
    if (this.opts.cookie.maxAge) {
      return;
    }

    if (this.opts.cookie.ephemeral) {
      this.expires = null;
    } else {
      var time = this.createdAt || new Date().getTime();
      // the cookie should expire when it becomes invalid
      // we add an extra second because the conversion to a date
      // truncates the milliseconds
      this.expires = new Date(time + this.duration + 1000);
    }
  },

  clearContent: function(keysToPreserve) {
    var that = this;
    Object.keys(this._content).forEach(function(k) {
      // exclude this key if it's meant to be preserved
      if (keysToPreserve && (keysToPreserve.indexOf(k) > -1)) {
        return;
      }
      delete that._content[k];
    });
  },

  reset: function(keysToPreserve) {
    this.clearContent(keysToPreserve);
    this.createdAt = new Date().getTime();
    this.duration = this.opts.duration;
    this.updateDefaultExpires();
    this.dirty = true;
    this.loaded = true;
  },

  // alias for `reset` function for compatibility
  destroy: function() {
    this.reset();
  },

  setDuration: function(newDuration, ephemeral) {
    if (ephemeral && this.opts.cookie.maxAge) {
      throw new Error("you cannot have an ephemeral cookie with a maxAge.");
    }
    if (!this.loaded) {
      this.loadFromCookie(true);
    }
    this.dirty = true;
    this.duration = newDuration;
    this.createdAt = new Date().getTime();
    this.opts.cookie.ephemeral = ephemeral;
    this.updateDefaultExpires();
  },

  // take the content and do the encrypt-and-sign
  // boxing builds in the concept of createdAt
  box: function() {
    return SAFE_COOKIE.encode(this.opts, this._content, this.duration, this.createdAt);
  },

  unbox: function(content) {
    this.clearContent();

    var unboxed = SAFE_COOKIE.decode(this.opts, content);
    if (!unboxed) {
      return;
    }

    var that = this;
    Object.keys(unboxed.content).forEach(function(k) {
      that._content[k] = unboxed.content[k];
    });

    this.createdAt = unboxed.createdAt;
    this.duration = unboxed.duration;
    this.updateDefaultExpires();
  },

  updateCookie: function() {
    if (this.isDirty()) {
      // support for adding/removing cookie expires
      this.opts.cookie.expires = this.expires;

      // flexible handling of cookie domain
      var cookieDomain = this.opts.cookie.domain;
      if(NI.isFunction(cookieDomain)) {
        cookieDomain = cookieDomain(this.req);
      }

      try {
        this.cookies.set(this.opts.cookieName, this.box(),
          NI.shallowClone(this.opts.cookie, { domain: cookieDomain }));
      } catch (x) {
        // this really shouldn't happen. Right now it happens if secure is set
        // but cookies can't determine that the connection is secure.
      }
    }
  },

  loadFromCookie: function(forceReset) {
    var cookie = this.cookies.get(this.opts.cookieName);
    if (cookie) {
      this.unbox(cookie);

      var expiresAt = this.createdAt + this.duration;
      var now = Date.now();
      // should we reset this session?
      if (expiresAt < now) {
        this.reset();
      // if expiration is soon, push back a few minutes to not interrupt user
      } else if (expiresAt - now < this.activeDuration) {
        this.createdAt += this.activeDuration;
        this.dirty = true;
        this.updateDefaultExpires();
      }
    } else {
      if (forceReset) {
        this.reset();
      } else {
        return false; // didn't actually load the cookie
      }
    }

    this.loaded = true;
    this.json = JSON.stringify(this._content);
    return true;
  },

  isDirty: function() {
    return this.dirty || (this.json !== JSON.stringify(this._content));
  }

};

Object.defineProperty(Session.prototype, 'content', {
  get: function getContent() {
    if (!this.loaded) {
      this.loadFromCookie();
    }
    return this._content;
  },
  set: function setContent(value) {
    Object.defineProperty(value, 'reset', {
      enumerable: false,
      value: this.reset.bind(this)
    });
    Object.defineProperty(value, 'destroy', {
      enumerable: false,
      value: this.destroy.bind(this)
    });
    Object.defineProperty(value, 'setDuration', {
      enumerable: false,
      value: this.setDuration.bind(this)
    });
    this._content = value;
  }
});

function clientSessionFactory(opts) {
  if (!opts) {
    throw new Error("no options provided, some are required");
  }

  if (!(opts.secret || (opts.encryptionKey && opts.signatureKey))) {
    throw new Error("cannot set up sessions without a secret "+
                    "or encryptionKey/signatureKey pair");
  }

  // defaults
  opts.cookieName = opts.cookieName || "session_state";
  opts.duration = opts.duration || 24*60*60*1000;
  opts.activeDuration = 'activeDuration' in opts ?
    opts.activeDuration : ACTIVE_DURATION;

  var encAlg = opts.encryptionAlgorithm || SAFE_COOKIE.DEFAULT_ENCRYPTION_ALGO;
  encAlg = encAlg.toLowerCase();
  if (!ENCRYPTION_ALGORITHMS[encAlg]) {
    throw new Error('invalid encryptionAlgorithm, supported are: '+
                    Object.keys(ENCRYPTION_ALGORITHMS).join(', '));
  }
  opts.encryptionAlgorithm = encAlg;

  var sigAlg = opts.signatureAlgorithm || SAFE_COOKIE.DEFAULT_SIGNATURE_ALGO;
  sigAlg = sigAlg.toLowerCase();
  if (!SIGNATURE_ALGORITHMS[sigAlg]) {
    throw new Error('invalid signatureAlgorithm, supported are: '+
                    Object.keys(SIGNATURE_ALGORITHMS).join(', '));
  }
  opts.signatureAlgorithm = sigAlg;

  // set up cookie defaults
  opts.cookie = opts.cookie || {};
  if (typeof opts.cookie.httpOnly === 'undefined') {
    opts.cookie.httpOnly = true;
  }

  // let's not default to secure just yet,
  // as this depends on the socket being secure,
  // which is tricky to determine if proxied.
  /*
  if (typeof(opts.cookie.secure) == 'undefined')
    opts.cookie.secure = true;
    */

  SAFE_COOKIE.setupKeys(opts);
  keyConstraints(opts);

  var propertyName = opts.requestKey || opts.cookieName;

  return function clientSession(req, res, next) {
    if (propertyName in req) {
      return next(); //self aware
    }

    var cookies = new Cookies(req, res);
    var rawSession;
    try {
      rawSession = new Session(req, res, cookies, opts);
    } catch (x) {
      // this happens only if there's a big problem
      process.nextTick(function() {
        next(new Error("client-sessions error: " + x.toString()));
      });
      return undefined;
    }

    Object.defineProperty(req, propertyName, {
      get: function getSession() {
        return rawSession.content;
      },
      set: function setSession(value) {
        if (NI.isObject(value)) {
          rawSession.content = value;
        } else {
          throw new TypeError("cannot set client-session to non-object");
        }
      }
    });


    var writeHead = res.writeHead;
    res.writeHead = function () {
      rawSession.updateCookie();
      return writeHead.apply(res, arguments);
    };

    return next();
  };
}

module.exports = clientSessionFactory;
