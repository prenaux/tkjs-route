var NI = require('tkjs-ni');

function createMulterUploaderAzure(aMulter,aOptions) {
  return require('./uploader_azure')(aOptions);
}

function createMulterUploaderS3(aMulter,aOptions) {
  NI.assert.has(aOptions,'accessKeyId');
  NI.assert.has(aOptions,'secretAccessKey');
  NI.assert.has(aOptions,'getStorageKey');
  var AWS = require('aws-sdk');
  var S3CLIENT = new AWS.S3({
    accessKeyId: aOptions.accessKeyId,
    secretAccessKey: aOptions.secretAccessKey
  });
  return require('./uploader_s3')(NI.assignClone({
    s3: S3CLIENT,
    bucket: 'dp.storeviva.com',
    acl: 'public-read',
    key: function (req, file, cb) {
      aOptions.getStorageKey(file,req).then(function(aFileKey) {
        cb(null, aFileKey);
      }).catch(NI.promiseCatch);
    }
  }, aOptions));
}

function createMulterUploaderDirectory(aMulter,aOptions) {
  var FS = require('tkjs-ni/sources/fs');
  var uploadDir = aOptions.uploadDir;
  if (NI.isEmpty(uploadDir)) {
    throw NI.Error("createMulterUploaderDirectory, no uploadDir specified.");
  }
  FS.mkdirpSync(uploadDir);
  return aMulter.diskStorage(NI.assignClone({
    destination: function (req, file, cb) {
      cb(null, uploadDir)
    },
    filename: function (req, file, cb) {
      aOptions.getStorageKey(file,req).then(function(aFileKey) {
        cb(null, aFileKey);
      }).catch(NI.promiseCatch);
    }
  },aOptions));
}

function createUploader(aOptions) {
  var multer = require("multer");
  var storage;
  if (aOptions.storageType == 'azure') {
    storage = createMulterUploaderAzure(multer,aOptions);
  }
  else if (aOptions.storageType == 's3') {
    storage = createMulterUploaderS3(multer,aOptions);
  }
  else if (aOptions.storageType == 'directory') {
    storage = createMulterUploaderDirectory(multer,aOptions)
  }
  else {
    throw NI.Error("Unknown uploader storageType: '%s'.",
                   aOptions.storageType);
  }
  var uploader = multer({ storage: storage }) ;
  return uploader;
}
exports.createUploader = createUploader;
