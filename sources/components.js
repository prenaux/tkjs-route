/* global __BACKEND__ */
var NI = require('tkjs-ni');

//----------------------------------------------------------------------
// Renderer
//----------------------------------------------------------------------
var _renderer = undefined;
function initRenderer(aComponentRenderer) {
  if (aComponentRenderer && (aComponentRenderer.renderToString || aComponentRenderer.renderToElement)) {
    _renderer = aComponentRenderer;
  }
  else {
    throw NI.Error("Invalid component renderer!");
  }
  return module.exports;
}
exports.initRenderer = initRenderer;

function initReactRenderer(aReact,aReactDOMClient,aReactDOMServer) {
  var _React = aReact;
  function getReact() {
    if (!_React) {
      throw NI.Error("No valid react object passed to initReactRenderer.");
    }
    return _React;
  }

  var _ReactDOMClient = aReactDOMClient;
  function getReactDOMClient() {
    if (!_ReactDOMClient) {
      if (getReact().render) { // < react 0.14
        _ReactDOMClient = getReact();
      }
      else {
        throw NI.Error("No valid react-dom object passed to initReactRenderer.");
      }
    }
    return _ReactDOMClient;
  }

  var _ReactDOMServer = aReactDOMServer;
  function getReactDOMServer() {
    if (!_ReactDOMServer) {
      if (getReact().renderToString) { // < react 0.14
        _ReactDOMServer = getReact();
      }
      else {
        throw NI.Error("No valid react-dom/server object passed to initReactRenderer.");
      }
    }
    return _ReactDOMServer;
  }

  return initRenderer({
    renderToString: function(aComponent, aProps) {
      return getReactDOMServer().renderToString(getReact().createElement(aComponent, aProps));
    },
    renderToElement: function(aEl, aComponent, aProps) {
      return getReactDOMClient().render(getReact().createElement(aComponent, aProps), aEl);
    },
  });
}
exports.initReactRenderer = initReactRenderer;

if (__BACKEND__) {
  // renderToString, server side
  exports.renderToString = function renderToString(aName, aProps) {
    var component = getComponent(aName);
    if (!component) {
      throw new Error("Couldn't find component: '" + aName + "'.");
    }
    return _renderer.renderToString(component, aProps);
  };
}
else {
  // renderToElement, client side
  exports.renderToElement = function renderToElement(aEl, aName, aProps) {
    var component = getComponent(aName);
    if (!component) {
      throw new Error("Couldn't find component: '" + aName + "'.");
    }
    return _renderer.renderToElement(aEl, component, aProps);
  };
}

//----------------------------------------------------------------------
// Register the components
//----------------------------------------------------------------------
var _components = {};

function registerComponent(aComponent) {
  if (NI.isArray(aComponent)) {
    NI.forEach(aComponent,function(c) {
      registerComponent(c);
    });
  }
  else {
    var className = aComponent && aComponent.className;
    NI.assert.isString(className);

    // clientSide is set by default
    if (!("clientSide" in aComponent)) {
      aComponent.clientSide = true;
    }

    // NI.log("Registered component: %s", className);
    if (!aComponent.displayName) {
      aComponent.displayName = className;
    }
    _components[className] = aComponent;
  }

  return module.exports;
}
exports.registerComponent = registerComponent;
exports.registerComponents = registerComponent;

function getComponent(aCompOrName) {
  return (typeof aCompOrName === "string") ? _components[aCompOrName] : aCompOrName;
}
exports.getComponent = getComponent;

//----------------------------------------------------------------------
// Server
//----------------------------------------------------------------------
if (__BACKEND__) { (function() {
  var ROUTE = require('./route');
  var PATH = require('tkjs-ni/sources/path');

  NI.global.renderComponentToString = exports.renderToString;

  function buildComponentParams(aName) {
    var c = getComponent(aName);
    var params = {
      title: c.title || NI.stringSpaceize(c.className),
      className: c.className,
      props: {},
      serverSide: c.serverSide,
      clientSide: c.clientSide,
      userData: c.userData || {},
    };
    return params;
  }

  function buildComponentRouteParams(aName) {
    var c = getComponent(aName);
    var route_params = "";
    if (c.route_params) {
      for (var rpi in c.route_params) {
        route_params += "/" + c.route_params[rpi];
      }
    }
    return route_params;
  }

  function GET_COMPONENT(aBaseName, aComponentParams, aACLRoute, aViewTemplate) {
    if (typeof(aComponentParams) === "string") {
      aComponentParams = buildComponentParams(aComponentParams);
    }
    try {
      ROUTE.GET_PAGE(aBaseName, aViewTemplate || 'component.html.ejs', {
        component: aComponentParams,
        renderComponentToString: exports.renderToString
      }, aACLRoute);
    }
    catch (e) {
      NI.error("Couldn't register component '%s' (param: %k): %K",
               aBaseName, aComponentParams, e)
      throw e;
    }
  }

  function registerComponentRoute(aRouteDefaultACL,c,params,allRoutesParams,routeDef) {
    var routeUrl;
    var routeACL = aRouteDefaultACL;
    var thisRouteParams = allRoutesParams;

    // gather the component's url, route params and acl
    if (typeof routeDef === "string") {
      routeUrl = routeDef;
    }
    else if (routeDef) {
      // url
      routeUrl = routeDef.url || routeUrl;
      // params
      if (routeDef.params) {
        thisRouteParams = buildComponentRouteParams(routeDef.params);
      }
      // ACL
      routeACL = routeDef.acl;
    }

    // finialize the route url
    if (!routeUrl) {
      routeUrl = '/c/'+c.className;
    }
    else if (routeUrl === "/" || routeUrl === "?" || routeUrl === "=") {
      routeUrl = '/c/'+c.className+routeUrl;
    }
    else if (routeUrl === "//") {
      routeUrl = '/';
    }

    // compute the final urls
    var routeComponentUrl, routeACLUrl;
    var isQueryRoute = NI.stringEndsWith(routeUrl,"?") || NI.stringEndsWith(routeUrl,"=");
    if (isQueryRoute) {
      // if the url contains a query mark, strip it, so that we get the "raw"
      // url to register the component
      routeComponentUrl = NI.stringRBefore(routeUrl,"?");
      routeACLUrl = routeUrl;
    }
    else {
      routeComponentUrl = routeUrl;
      routeACLUrl = routeUrl;
    }

    if (thisRouteParams.length > 0) {
      routeComponentUrl = PATH.join(routeComponentUrl, thisRouteParams);
      if (!NI.stringEndsWith(routeACLUrl,"/")) {
        routeACLUrl += "/";
      }
    }

    // NI.trace("routeUrl[%s]: comp: %s, acl: %s, routeDef: %k", c.className, routeComponentUrl, routeACLUrl, routeACL);

    ROUTE.ACL.registerACL(routeACLUrl, routeACL || "__public__");
    GET_COMPONENT(routeComponentUrl, params, routeACLUrl, c.viewTemplate);
  }

  function registerComponentsRoutes(aRouteDefaultACL) {
    for (var key in _components) {
      var c = _components[key];
      var params = buildComponentParams(c);
      var allRoutesParams = buildComponentRouteParams(c);
      if (c.routes) {
        for (var idx in c.routes) {
          registerComponentRoute(aRouteDefaultACL, c, params, allRoutesParams, c.routes[idx]);
        }
      }
      else {
        registerComponentRoute(aRouteDefaultACL, c, params, allRoutesParams);
      }
    }
    return module.exports;
  }

  exports.registerComponentsRoutes = registerComponentsRoutes;
})(); }
