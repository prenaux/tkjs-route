/* global __BACKEND__ */
var NI = require('tkjs-ni');
var MODEL = require("tkjs-model");
var OS = require('os');
var FS = require('tkjs-ni/sources/fs');
var PATH = require('tkjs-ni/sources/path');
var URL = require('url');

if (typeof __BACKEND__ === 'undefined' || !__BACKEND__) {
  throw NI.Error("Invalid __BACKEND__ definition.");
}
if ((typeof NI.global.isProduction === 'undefined') || (NI.global.isProduction !== (process.env.NODE_ENV === 'production'))) {
  throw NI.Error("Invalid global.isProduction definition.");
}

function getSession(req) {
  if (!req) {
    return {};
  }
  var session = req[global.COOKIE_NAME] || {};
  if (exports.getSessionHook) {
    exports.getSessionHook(session);
  }
  if (NI.isNotEmpty(session.user_name) && !session.user_acl) {
    session.user_acl = {};
    NI.warn("No ACL in user session cookie, added an empty one: %K", session);
  }
  return session;
};
exports.getSession = getSession;
exports.getSessionHook = undefined;

function getPageRenderObject(req, aTemplateParams) {
  // retrieve the current session object
  var session = getSession(req);
  // make the request parameters an object so that they can be passed around
  var params = {};
  if (req) {
    for (var x in req.params) {
      params[x] = req.params[x];
    }
  }

  var checkSessionACL = function(aRequiredAC) {
    return ACL.checkUserACL(session.user_acl,aRequiredAC)
  };
  global.checkSessionACL = checkSessionACL;

  var obj = {
    layout: false,
    appConfig: appConfig,
    session: session,
    checkSessionACL: checkSessionACL,
    global: global,
    url: req ? req.url : "",
    params: params
  };
  if (aTemplateParams) {
    NI.forEach(aTemplateParams, function(val,key) {
      obj[key] = val;
    });
  }
  return obj;
}
exports.getPageRenderObject = getPageRenderObject;

function renderHtml(req, res, aPage, aParams) {
  res.render(aPage, getPageRenderObject(req,aParams));
}
exports.renderHtml = renderHtml;

function getRequestIP(req) {
  var ip = 'noip';
  if (req.headers) {
    ip = req.headers['x-forwarded-for']
    if (ip) {
      return ip;
    }
  }
  if (req.connection) {
    ip = req.connection.remoteAddress;
    if (ip) {
      return ip;
    }
  }
  return ip;
}
exports.getRequestIP = getRequestIP;

function SERVER_ERROR(req, res, err, aOptions) {
  aOptions = aOptions || {};
  aOptions.renderRaw = ("renderRaw" in aOptions) ? aOptions.renderRaw : false;
  aOptions.includeStack = ("includeStack" in aOptions) ? aOptions.includeStack : false;
  aOptions.sendCrashReport = ("sendCrashReport" in aOptions) ? aOptions.sendCrashReport : false;

  // set and validate the status code
  if (aOptions.status) {
    res.statusCode = NI.parseInt(aOptions.status);
  }
  if (!res.statusCode || res.statusCode < 400) {
    res.statusCode = 500;
  }
  res.status(res.statusCode);

  // always log, include the stack and sendCrashReport for unexpected errors
  if (res.statusCode >= 500) {
    aOptions.noLog = false;
    aOptions.includeStack = true;
    aOptions.sendCrashReport = true;
  }

  var errExt, errDesc;
  if (err && err.desc && err.ext) {
    errDesc = err.desc;
    errExt = err.ext;
  }
  else {
    errDesc = NI.formatError(err || "UndefinedError");
  }

  var errObj = {
    status: res.statusCode,
    reqUrl: req.url,
    fromIP: getRequestIP(req),
    desc: errDesc || "UndefinedError",
    fullUrl: req.protocol + '://' + req.get('host') + req.originalUrl,
    payload: getRequestPayload(req),
    stack: (aOptions.includeStack && err && err.stack) ?
      ((""+err.stack).split('\n')) : undefined,
  };
  if (errExt) {
    errObj = NI.assignShallowClone(errObj, errExt);
  }
  if (res.statusCode === 403) {
    errObj.error = 'Access Forbidden';
  }
  else if (res.statusCode === 404) {
    errObj.error = 'Not Found';
  }
  else if (res.statusCode === 400) {
    errObj.error = 'Bad Request';
  }
  else {
    errObj.error = 'Unexpected Server Error';
  }

  if (!aOptions.noLog) {
    NI.logEx(
      NI.format("Server Error: %J %s", errObj, NI.logGetCallee(2)),
      "error");
  }

  var renderError = function() {
    var publicErrObj = {
      error: errObj.error,
      status: errObj.status,
      desc: errObj.desc
    };
    if (aOptions.includeStack) {
      publicErrObj.stack = errObj.stack;
    }

    // respond with a html page
    if (!req ||
        (req.accepts('html') &&
         (!NI.stringStartsWith(req.url, "/api"))))
    {
      renderHtml(req, res, 'status_error.html.ejs', {
        layout: false,
        url: errObj.url,
        error: errObj.error,
        status: errObj.status,
        desc: errObj.desc
      });
      return;
    }
    // respond with json
    if (req && req.accepts('json')) {
      res.setHeader('Content-Type', 'application/json');
      res.send(publicErrObj);
      return;
    }
    // default to plain-text. send()
    res.end(NI.format("Server Error: %J", publicErrObj));
  };

  var sendCrashReport = aOptions.sendCrashReport;
  if (aOptions.renderRaw) {
    res.write("<div class='server-error server-error-msg'>");
    res.write("<pre class='server-error server-error-status'>"+errObj.status+": "+errObj.error+"</pre>");
    res.write("<pre class='server-error server-error-desc'>"+errObj.desc+"</pre>");
    res.write("</div>");
    res.end();
  }
  else if (errObj.status >= 400 && errObj.status <= 499) {
    renderError();
  }
  else {
    sendCrashReport = true;
  }

  if (sendCrashReport) {
    var mailContent = NI.format("Server Error: %J", errObj);
    appConfig.sendCrashReport(
      ("Server crash: " +
       errObj.status + ", " +
       errObj.error + ", " +
       errObj.url + ", " +
       "from " + errObj.fromIP),
      mailContent,
      function (e) {
        if (e) {
          NI.log("sendCrashReport failed: " + e);
        }
        renderError();
      });
  }
}
exports.SERVER_ERROR = SERVER_ERROR;

global._routes_get = {};
function GET(aUrl, aCallback) {
  if (!ACL.findACL(aUrl)) {
    throw NI.Error("ROUTE.GET: No ACL found for route: %s", aUrl);
  }

  global._routes_get[aUrl] = aUrl;
  // NI.log("... _GET: " + aUrl);
  APP.get(aUrl, function (req, res) {
    try {
      aCallback(req, res);
    }
    catch (e) {
      SERVER_ERROR(req, res, e);
    }
  });
}
exports.GET = GET;

function GET_STATIC(aUrl, aDir) {
  if (!ACL.findACL(aUrl)) {
    throw NI.Error("ROUTE.GET: No ACL found for route: %s", aUrl);
  }
  APP.use(aUrl, EXPRESS.static(aDir));
}
exports.GET_STATIC = GET_STATIC;

global._routes_post = {};
function POST(aUrl, aCallback) {
  if (!ACL.findACL(aUrl)) {
    throw NI.Error("ROUTE.POST: No ACL found for route: %s", aUrl);
  }

  global._routes_post[aUrl] = aUrl;
  // NI.log("... _POST: " + aUrl);
  APP.post(aUrl, function (req, res) {
    try {
      aCallback(req, res);
    }
    catch (e) {
      SERVER_ERROR(req, res, e);
    }
  });

  // record a GET aswell so that we can test the request easily
  if (appConfig.alsoRegisterPostAsGet) {
    APP.get(aUrl, function (req, res) {
      try {
        aCallback(req, res);
      }
      catch (e) {
        SERVER_ERROR(req, res, e);
      }
    });
  }
}
exports.POST = POST;

function GET_LOGS(aLogsRoute, aLogsDir) {
  var MOMENT = require('tkjs-ni/sources/moment');

  aLogsDir = aLogsDir || getAppConfig().logsDir;

  GET_STATIC(aLogsRoute + '/files/', aLogsDir);

  GET(aLogsRoute + '/list', function(req, res) {
    NI.series([
      function() {
        res.set('Content-Type', 'text/html');
        res.write("<h1>Logs</h1>");
        res.write("<ul>");
      },
      function() {
        NI.forEach(FS.walkSync(aLogsDir), function(file,index) {
          var url = PATH.join(aLogsRoute, '/files/', NI.stringAfter(file, aLogsDir))
          var statString = "";
          try {
            var filePath = file;
            var stats = FS.statSync(filePath);
            statString = "mtime: " + MOMENT(stats.mtime).format() + ", size: " + stats.size;
          }
          catch (e) {
            statString = "Can't stat '" + file + "': " + e;
          }
          res.write("<li>"+index+": <a href='"+url+"'>"+file+"</a>: "+statString+"</li>");
        });
      },
      function() {
        res.write("</ul>");
        res.end();
      },
    ]).catch(function(e) {
      SERVER_ERROR(req, res, e, { renderRaw: true });
    });
  });
}
exports.GET_LOGS = GET_LOGS;

function removeUploadedFile(path) {
  // Delete the Temp File
  var filePath = path;
  FS.unlink(filePath, function(err) {
    if (err) {
      NI.log("Can't delete temp file: %k", err);
    }
    else {
      NI.log("RemovedTempFile: %s", filePath);
    }
  });
}
function removeUploadedFiles(aFiles) {
  NI.forEach(aFiles,function(file) {
    removeUploadedFile(file.path);
  });
}
exports.removeUploadedFiles = removeUploadedFiles;

function POST_FILES(aUrl, aCallback, aOnFinalize) {
  NI.assert.isString(appConfig.dirFileUploads);

  var MULTER = require('multer');
  if (!ACL.findACL(aUrl)) {
    throw NI.Error("ROUTE.POST: No ACL found for route: %s", aUrl);
  }

  global._routes_post[aUrl] = aUrl;
  // NI.log("... _POST: " + aUrl);

  var multer = MULTER({
    dest: global.ROOT_DIR + appConfig.dirFileUploads
  }).array('fileupload',appConfig.maxFileUploads)

  APP.post(aUrl, multer, function (req, res) {
    var finalizeData = {};

    var onFinalize = function() {
      if (aOnFinalize) {
        try {
          aOnFinalize(req,res,finalizeData);
          NI.info("POST_FILES finalized.");
        }
        catch (e) {
          NI.error("POST_FILES finalize error: " + e);
        }
      }
      if (appConfig.fileUploadsAutoCleanup) {
        removeUploadedFiles(req.files);
      }
    };

    try {
      aCallback(req,res,finalizeData,onFinalize);
    }
    catch (e) {
      SERVER_ERROR(req, res, e);
      onFinalize();
    }
  });
}
exports.POST_FILES = POST_FILES;

global._routes_page = {};
function GET_PAGE(aBaseName, aHtml, aTemplateParams, aACLRoute) {
  NI.assert.isTrue(NI.stringStartsWith(aBaseName,"/"));
  var route = aBaseName;
  if (route in global._routes_page) {
    throw NI.Error('Page Route already registered:' + route)
  }

  aHtml = aHtml || (aBaseName + ".html.ejs");
  global._routes_page[route] = aHtml.toString();
  // NI.log("... _GET_PAGE: '" + route + "' -> " + aHtml);

  if (aACLRoute &&
      !NI.stringStartsWith(route,aACLRoute) &&
      !NI.stringStartsWith(aACLRoute,route))
  {
    throw NI.Error("ROUTE.GET_PAGE: The specified route and ACL route are not compatible: route: %s, aclroute: %s", aACLRoute, route);
  }

  if (!ACL.findACL(aACLRoute || route)) {
    throw NI.Error("ROUTE.GET_PAGE: No ACL found for page: %s, %s", aBaseName, route);
  }

  APP.get(route, function (req, res) {
    try {
      var tplParams;
      if (NI.isFunction(aTemplateParams)) {
        tplParams = aTemplateParams();
      }
      else {
        tplParams = aTemplateParams;
      }
      renderHtml(req, res, aHtml, tplParams);
    }
    catch (e) {
      SERVER_ERROR(req, res, e);
    }
  });
}
exports.GET_PAGE = GET_PAGE;

function GET_REDIRECT(aShortName, aRedirectTo) {
  NI.assert.isTrue(NI.stringStartsWith(aShortName,"/"));
  NI.assert.isTrue(NI.stringStartsWith(aRedirectTo,"/"));
  // NI.log("... _GET_REDIRECT: '" + aShortName + "' -> " + aRedirectTo);
  GET(aShortName, function (req, res) {
    res.redirect(aRedirectTo);
  });
}
exports.GET_REDIRECT = GET_REDIRECT;

function API(aOptions) {
  NI.assert.has(aOptions,"model");
  var model = aOptions.model;
  if (typeof model === "string") {
    model = MODEL.getModel(model);
  }

  var routeApiUrl, routeAclUrl;
  var routeUrl = aOptions.get || aOptions.post;
  if (!routeUrl) {
    throw NI.Error("No URL specified for API.");
  }
  if (NI.stringContains(routeUrl,":")) {
    routeApiUrl = routeUrl;
    routeAclUrl = NI.stringBefore(routeUrl,":");
  }
  else {
    routeApiUrl = routeUrl;
    routeAclUrl = routeUrl;
  }

  var aclOwnerCheck = aOptions.acl_owner_field;
  var acl = aOptions.acl;
  if (acl === undefined) {
    throw NI.Error("No ACL specified for API '%s'.", routeUrl);
  }
  if (acl === "__owner__") {
    acl = true;
    if (NI.isEmpty(aclOwnerCheck)) {
      throw NI.Error("No ACL owner field specified for API '%s'.", routeUrl);
    }
  }
  else {
    if (!NI.isEmpty(aclOwnerCheck)) {
      throw NI.Error("ACL owner field specified for API '%s' but ACL is not __owner__.", routeUrl);
    }
  }
  ACL.registerACL(routeAclUrl, acl || "__public__");

  var handler;
  if (aOptions.onHandle) {
    handler = aOptions.onHandle;
  }
  else if (aOptions.onPromise) {
    handler = function(aReq,aRes,aPayload,aSession) {
      try {
        var promise = aOptions.onPromise(aReq,aRes,aPayload,aSession);
        if (!NI.isPromise(promise)) {
          throw NI.Error("onPromise didnt return a promise for API '%s'.", routeUrl);
        }
        promise.then(function(aResult) {
          if (aResult.__redirect) {
            aRes.redirect(aResult.__redirect);
          }
          else {
            OK(aRes, aResult);
          }
        }).catch(function(aError) {
          SERVER_ERROR(aReq, aRes, aError, { status: 400 });
        });
      }
      catch (aError) {
        SERVER_ERROR(aReq, aRes, aError);
      }
    };
  }
  else {
    throw NI.Error("No onHandle nor onPromise specified for API '%s'.", routeUrl);
  }

  (aOptions.get ? GET : POST)(routeApiUrl, function(req,res) {
    var session, payload;
    try {
      session = getSession(req);
      payload = getRequestPayload(req);
      if (aOptions.onPreprocessPayload) {
        payload = aOptions.onPreprocessPayload(payload);
      }
      payload = MODEL.assertValidate(payload,model);
    }
    catch (e) {
      return SERVER_ERROR(req,res,e,{ status: 400 });
    }
    if (aclOwnerCheck) {
      var ownerName = payload[aclOwnerCheck];
      if (NI.isEmpty(ownerName)) {
        return SERVER_ERROR(req,res,"No owner set in the payload with field '" + aclOwnerCheck + "'.", { status: 403 })
      }
      if (!ACL.checkIsUser(ownerName, session)) {
        return SERVER_ERROR(req,res,"Only the owner can access this route.", { status: 403 });
      }
    }
    return handler(req,res,payload || {},session);
  });
}
exports.API = API;

function OK(aRes, aPayload) {
  aRes.send({
    OK: aPayload
  });
}
exports.OK = OK;

function ERROR(aRes, aError) {
  aRes.status(400).send({
    error: NI.formatError(aError),
  });
}
exports.ERROR = ERROR;

function getRequestPayload(req) {
  return NI.assignClone(
    req.params || {},
    req.query,
    req.body);
}
exports.getRequestPayload = getRequestPayload;

//----------------------------------------------------------------------
// MQ APIs
//----------------------------------------------------------------------
function MQ(aMQ,aMsgId,aHandlerCallback) {
  NI.assert.has(aMQ,"_messages");
  var msgDesc = aMQ._messages[aMsgId];
  if (!msgDesc) {
    throw NI.Error("Can't find message desc '%s' in specified MQ.",aMsgId);
  }

  var messageHandler;
  for (var i = 0; i < msgDesc.handlers.length; ++i) {
    messageHandler = msgDesc.handlers[i];
    if (messageHandler.get || messageHandler.post) {
      break;
    }
    messageHandler = undefined;
  }
  if (!messageHandler) {
    throw NI.Error(
      "Message '%s' doesn't have a handler with a a get or post api exposed.",
      aMsgId);
  }

  var handlerKind = messageHandler.get ? "get" : "post";
  var model = messageHandler.model;
  if (!model) {
    throw NI.Error(
      "Message '%s' with %s api doesn't have a model specified.",
      aMsgId,handlerKind);
  }
  if (typeof aModel === "string") {
    model = MODEL.getModel(model);
  }
  if (messageHandler.get) {
    GET(messageHandler.get, function(req,res) {
      var payload = MODEL.assertValidate(getRequestPayload(req),model);
      return aHandlerCallback(req,res,payload);
    });
  }
  else if (messageHandler.post) {
    POST(messageHandler.post, function(req,res) {
      var payload = MODEL.assertValidate(getRequestPayload(req),model);
      return aHandlerCallback(req,res,payload);
    });
  }
}
exports.MQ = MQ;

function MQ_SQL(aMQ,aMsgId,aSQL,aSPName,aGetSPParams) {
  MQ(aMQ, aMsgId, function(req,res,payload) {
    aSQL.call(
      aSPName, aGetSPParams(payload,req,res),
      function(aDBReply) {
        OK(res, {
          num_items: aDBReply.length,
          items: aDBReply
        })
      }, res);
  });
}
exports.MQ_SQL = MQ_SQL;

//----------------------------------------------------------------------
// UPLOAD APIs
//----------------------------------------------------------------------
/** Example:

  var storageOptions;
  if (false) {
    storageOptions = {
      storageType: 'azure',
      azureStorageConnectionString: 'https://mystorageaccount.blob.core.windows.net/',
      azureStorageAccount: "myaccountname",
      azureStorageAccessKey: "myaccesskey",
      containerName: "mycontainer",
      getStorageKey: function(file) {
        return NI.resolve('' + Date.now() + '-' + file.originalname);
      }
    }
  }
  else if (false) {
    storageOptions = {
      storageType: 's3',
      accessKeyId: "accessKeyId",
      secretAccessKey: "secretAccessKey",
      getStorageKey: function(file) {
        return NI.resolve('' + Date.now() + '-' + file.originalname);
      }
    }
  }
  else {
    storageOptions = {
      storageType: 'directory',
      uploadDir: PATH.join(__dirname, '../uploads'),
      getStorageKey: function(file) {
        return NI.resolve('' + Date.now() + '-' + file.originalname);
      }
    }
  }

  ROUTE.ACL.addRouteStartsWith([
    { name: "/api/1/upload/", canUse: ROUTE.ACL.canUse.public },
  ]);

  // Example: curl -v -F file=@Clipboard01.jpg http://localhost:1388/api/1/upload/single
  ROUTE.UPLOAD(
    '/api/1/upload/single',
    NI.assignClone(storageOptions,{
      uploadType: 'single',
      uploadFieldName: 'file'
    }),
    function(aReq,aRes) {
      ROUTE.OK(aRes, { upload: aReq.file });
    });

 */

function UPLOAD(aUrl,aOptions,aCallback) {
  NI.assert.has(aOptions,"model");
  var model = aOptions.model;
  if (typeof model === "string") {
    model = MODEL.getModel(model);
  }

  var uploader = require('./uploader').createUploader(aOptions);
  var doUpload;
  if (aOptions.uploadType == 'single') {
    doUpload = uploader.single(aOptions.uploadFieldName || 'file');
  }
  else {
    throw NI.Error("Unknown uploadType '%s'.", aOptions.uploadType);
  }

  POST(aUrl, function(aReq, aRes) {
    var payload;
    try {
      payload = MODEL.assertValidate(getRequestPayload(aReq),model)
    }
    catch (e) {
      return SERVER_ERROR(aReq,aRes,e,{ status: 400 });
    }

    NI.info("UPLOAD: Uploading on url: %s, options: %j, payload: %j.",
            aUrl, aOptions, payload);
    return doUpload(aReq, aRes, function(aErr) {
      if (aErr) {
        NI.info("UPLOAD: Error '%s' on url: %s, options: %j, payload: %j.",
                aErr, aUrl, aOptions, payload);
        ERROR(aRes, NI.format("%s", aErr));
        return;
      }
      try {
        aCallback(aReq,aRes,payload);
      }
      catch (e) {
        SERVER_ERROR(aReq, aRes, e);
      }
    });
  });
}
exports.UPLOAD = UPLOAD;

//----------------------------------------------------------------------
// HTTPS
//----------------------------------------------------------------------
var parseUrl = require('url').parse;

var isSecureRequest = function(req) {
  if (req.secure ||
      req.headers['x-arr-ssl'] ||
      (req.get('X-Forwarded-Proto') &&
       req.get('X-Forwarded-Proto').toLowerCase &&
       req.get('X-Forwarded-Proto').toLowerCase() === 'https')
     )
  {
    return true;
  }
  return false;
};
exports.isSecureRequest = isSecureRequest;

function redirectToHttpsMiddleware(req, res, next) {
  if (!isSecureRequest(req)) {
    if (req.method === "GET") {
      var httpsPort = appConfig.httpsPort || 443;
      var fullUrl = parseUrl(
        req.protocol + '://' +
          (appConfig.httpsDomain || req.header('Host')) +
          req.originalUrl
      );
      res.redirect(
        301, 'https://' + fullUrl.hostname + ':' + httpsPort + req.originalUrl);
    }
    else {
      res.status(403).send('HTTPS Required.');
    }
  }
  else {
    next();
  }
};
exports.redirectToHttpsMiddleware = redirectToHttpsMiddleware;

function redirectToDomainMiddleware(req, res, next) {
  if (appConfig.redirectAllToDomain) {
    var forceUrl = appConfig.redirectAllToDomain;
    var forceHost = URL.parse(forceUrl).host;
    var reqHost = req.header("host");
    if (reqHost == forceHost) {
      next();
    }
    else {
      res.redirect(301, forceUrl+req.path);
    }
  }
  else {
    next();
  }
};
exports.redirectToDomainMiddleware = redirectToDomainMiddleware;

//----------------------------------------------------------------------
// ACL / Auth Check
//----------------------------------------------------------------------
var ACL = require('./acl');
exports.ACL = ACL;

function checkAuthMiddleware(req, res, next) {
  var url = req.url;
  // console.log("... checkAuthMiddleware: " + url);

  var aclRoute = ACL.findACL(url);
  if (!aclRoute) {
    return SERVER_ERROR(req,res,"No ACL defined for this route.", { status: 403 });
  }

  var aclCheck = {
    route: aclRoute,
    req: req,
    res: res,
    session: getSession(req),
    onGranted: function(/*aACLRoute*/) {
      next();
    },
    onRedirect: function(aACLRoute,aRedirectTo) {
      res.redirect(aRedirectTo);
    },
    onForbbiden: function(aACLRoute,reason) {
      SERVER_ERROR(req,res,reason, { status: 403 });
    }
  }

  return ACL.checkACL(aclCheck);
}
exports.checkAuthMiddleware = checkAuthMiddleware;

//----------------------------------------------------------------------
// Initialization
//----------------------------------------------------------------------
var appConfig = {
  sendCrashReport: function sendCrashReport(aTitle, aMessage, aCallback) {
    // passhtrough by default
    aCallback();
  },
  catchUncaughtException: true,
  alsoRegisterPostAsGet: true,
  dirStaticBuild: "/static/build",
  dirStatic: "/static",
  dirViews: "/sources/views",
  dirFileUploads: "/uploads",
  maxFileUploads: 16,
  fileUploadsAutoCleanup: true,
  redirectAllToHttps: false, // false, true, 'always' (redirect in dev mode aswell)
};

function getAppConfig() {
  return appConfig;
}
exports.getAppConfig = getAppConfig;
global.getAppConfig = getAppConfig;

function getNetworkAddresses() {
  var interfaces = OS.networkInterfaces();
  var addresses = [];
  for (var k in interfaces) {
    for (var k2 in interfaces[k]) {
      var address = interfaces[k][k2];
      if ((address.family === 'IPv4' || address.family === 'IPv6') && !address.internal) {
        addresses.push(address.address);
      }
    }
  }
  addresses = addresses.sort();
  return addresses;
}
exports.getNetworkAddresses = getNetworkAddresses;

function initConfig(aConfig) {
  appConfig.isProduction = global.isProduction = (process.env.NODE_ENV === 'production');
  appConfig.isTestServer = global.isTestServer = (process.env.NODE_ENV === 'test');
  appConfig.isDev = global.isDev = (process.env.NODE_ENV === 'development');
  appConfig.networkAddresses = getNetworkAddresses();

  if (NI.isFunction(aConfig)) {
    aConfig = aConfig();
  }
  NI.assert.has(aConfig,"rootDir");
  NI.assert.has(aConfig,"timeZone");
  NI.assert.has(aConfig,"devPort");
  NI.assert.has(aConfig,"appName");
  NI.assert.has(aConfig,"appCopyright");
  NI.assert.has(aConfig,"domain");
  NI.assert.has(aConfig,"cookieName");
  NI.assert.has(aConfig,"cookieDomain");
  NI.assert.has(aConfig,"sessionSecret");
  NI.assert.has(aConfig,"sessionDuration");
  NI.assert.has(aConfig,"redirectAllToHttps");
  NI.assert.has(aConfig,"redirectAllToDomain");
  appConfig = NI.assignClone(appConfig, aConfig);

  if (appConfig.logsRoute) {
    NI.assert.has(aConfig,"logsDir");
  }

  global.ROOT_DIR = aConfig.rootDir;

  process.env.TZ = aConfig.timeZone;
  global.TZ = aConfig.timeZone;

  global.APP_NAME = aConfig.appName;
  global.APP_COPYRIGHT = aConfig.appCopyright;

  appConfig.serverIP = global.serverIP = (aConfig.serverIP || '0.0.0.0');
  appConfig.serverPort = global.serverPort = (process.env.PORT || aConfig.devPort);
  if (!global.isProduction) {
    appConfig.bundlePort = global.bundlePort = (global.serverPort + 1);
  }

  global.COOKIE_NAME = aConfig.cookieName;
  global.COOKIE_DOMAIN = aConfig.cookieDomain;

  return appConfig;
}
exports.initConfig = initConfig;

//----------------------------------------------------------------------
// application/graphql mime type handler
//----------------------------------------------------------------------
function bodyParserApplicationGraphQL(options) {
  var opts = options || {}
  var read = require('body-parser/lib/read');
  var debug = function(/*aMsg*/) {
    // NI.log("... bodyParserApplicationGraphQL: " + aMsg)
  }

  var defaultCharset = opts.defaultCharset || 'utf-8'
  var inflate = opts.inflate !== false
  var limit = opts.limit || (100 * 1024); // 100kb limit
  var type = opts.type || 'application/graphql'
  var verify = opts.verify || false

  var checkType = (typeof type === "function") ? type : function(aContentType) {
    return aContentType == type;
  };

  function parse(buf) {
    NI.log("... PARSE: '%s'", buf)
    return { query: buf }
  }

  function hasBody(req) {
    return req.headers['transfer-encoding'] !== undefined
      || !isNaN(req.headers['content-length'])
  }

  return function graphqlParser(req, res, next) {
    if (req._body) {
      return debug('body already parsed'), next()
    }
    req.body = req.body || {}

    // skip requests without bodies
    if (!hasBody(req)) {
      return debug('skip empty body'), next()
    }

    debug('content-type %j', req.headers['content-type'])

    // determine if request should be parsed
    var contentType = (req.headers['content-type'] && req.headers['content-type'].toLowerCase());
    if (!checkType(contentType)) {
      return debug('skip parsing'), next()
    }

    // get charset
    var charset = defaultCharset

    // read
    return read(req, res, next, parse, debug, {
      encoding: charset,
      inflate: inflate,
      limit: limit,
      verify: verify
    })
  }
}

//----------------------------------------------------------------------
// Server startup
//----------------------------------------------------------------------
var EXPRESS = undefined;
var APP = undefined;
var SERVER = undefined;

function initExpressAndApp(aExpress,aApp,aServer) {
  NI.invariant(
    typeof appConfig.appName === "string",
    "Config hasn't been initialized, you must call initConfig() first.");

  if (!EXPRESS) {
    EXPRESS = aExpress || require('express');
  }
  if (!APP) {
    APP = aApp || EXPRESS();
    NI.log("global.ROOT_DIR: " + global.ROOT_DIR);
    NI.log("global.isProduction: " + global.isProduction);
    NI.log("process.env.TZ: " + process.env.TZ);
  }
  if (!SERVER) {
    SERVER = aServer || require('http').createServer(APP);
  }
  return this;
}
exports.initExpressAndApp = initExpressAndApp;

function getExpress() {
  return EXPRESS;
}
exports.getExpress = getExpress;

function getApp() {
  return APP;
}
exports.getApp = getApp;

function getServer() {
  return SERVER;
}
exports.getServer = getServer;

function startServer(aOnSetupRoutes) {
  var startTime0 = NI.timerInSeconds(), startTime;
  var times = {};

  //--- Base startup
  NI.log("---- SERVER START ---------------------------------------------");
  NI.log("NODE_PATH: %s", process.env.NODE_PATH);
  NI.log("NODE_ENV: %s", process.env.NODE_ENV);

  NI.invariant(
    typeof appConfig.appName === "string",
    "Config hasn't been initialized, you must call initConfig() first.");
  NI.invariant(
    typeof aOnSetupRoutes === "function",
    "aOnSetupRoutes callback not specified.");

  //--- Log is initialized, log a starting message.
  NI.log("Server starting...");

  //--- Initialize express
  startTime = NI.timerInSeconds();
  initExpressAndApp();
  times["initExpressAndApp"] = NI.timerInSeconds()-startTime;

  //--- Check NODE_ENV
  var NODE_ENV = process.env.NODE_ENV;
  if (NODE_ENV != "production" && NODE_ENV != "development" && NODE_ENV != "test") {
    var errMsgNodeEnv = NI.format("ROUTE: NODE_ENV set to '%s'.",NODE_ENV);
    NI.error(errMsgNodeEnv);
    initExpressAndApp();
    APP.get('*', function(req, res, next) {
      res.status(400).send({
        error: errMsgNodeEnv
      });
      res.end();
      next();
    });
    APP.listen(global.serverPort,global.serverIP);
    return undefined;
  }

  //----------------------------------------------------------------------
  // Handle uncaught exception
  //----------------------------------------------------------------------
  if (appConfig.catchUncaughtException) {
    var hasPendingUncaughtException = undefined;
    process.on('uncaughtException', function (exc) {
      if (hasPendingUncaughtException == exc) {
        NI.error("hasPendingUncaughtException: " + exc);
        throw exc;
      }
      NI.error(
        "uncaughtException: %s\nstack:\n%s",
        exc, (exc&&exc.stack)?exc.stack:"<no stack>"
      );
      appConfig.sendCrashReport(
        "Server crash: uncaughtException",
        exc,
        function (scrErr) {
          if (scrErr) {
            NI.log("sendCrashReport failed: " + scrErr);
          }
          hasPendingUncaughtException = scrErr;
          throw exc;
        });
    });
  }

  {
    startTime = NI.timerInSeconds();

    //----------------------------------------------------------------------
    // Views setup
    //----------------------------------------------------------------------
    APP.set('views', global.ROOT_DIR + appConfig.dirViews);
    APP.set('view engine', 'ejs');
    APP.engine('.ejs', require('ejs').__express);

    //----------------------------------------------------------------------
    // Body parser
    //----------------------------------------------------------------------
    var bodyParser = require('body-parser');
    APP.use(bodyParserApplicationGraphQL());
    APP.use(bodyParser.json());
    APP.use(bodyParser.urlencoded({ extended: true }));

    //----------------------------------------------------------------------
    // Redirect to Domain
    //----------------------------------------------------------------------
    NI.log("appConfig.redirectAllToDomain: " + appConfig.redirectAllToDomain);
    if (appConfig.redirectAllToDomain) {
      if (!NI.stringContains(appConfig.redirectAllToDomain, appConfig.domain)) {
        throw NI.Error("redirectAllToDomain '%s' doesnt include the domain '%s'",
                       appConfig.redirectAllToDomain, appConfig.domain);
      }
      if (!NI.stringContains(appConfig.redirectAllToDomain, appConfig.cookieDomain)) {
        throw NI.Error("redirectAllToDomain '%s' doesnt include the cookieDomain '%s'",
                       appConfig.redirectAllToDomain, appConfig.cookieDomain);
      }
      APP.use(redirectToDomainMiddleware);
    }

    //----------------------------------------------------------------------
    // Redirect to HTTPS
    //----------------------------------------------------------------------
    NI.log("appConfig.redirectAllToHttps: " + appConfig.redirectAllToHttps);
    NI.log("appConfig.httpsDomain: " + appConfig.httpsDomain);
    if (appConfig.redirectAllToHttps) {
      APP.use(redirectToHttpsMiddleware);
    }

    //----------------------------------------------------------------------
    // Session setup
    //----------------------------------------------------------------------
    var sessionMiddleware = require('./safe_cookie_session');
    var sessionConfig = {
      secret: appConfig.sessionSecret,
      cookieName: appConfig.cookieName,
      duration: appConfig.sessionDuration,
      cookie: {}
    };
    NI.log("COOKIE_NAME: %s", global.COOKIE_NAME);
    NI.log("COOKIE_DOMAIN: %s", global.COOKIE_DOMAIN);
    if (global.COOKIE_DOMAIN) {
      sessionConfig.cookie.domain = global.COOKIE_DOMAIN;
    }
    APP.use(sessionMiddleware(sessionConfig));

    times["viewSessionSetup"] = NI.timerInSeconds()-startTime;
  }

  //----------------------------------------------------------------------
  // ACL & Auth
  //----------------------------------------------------------------------
  {
    startTime = NI.timerInSeconds();

    ACL.init();
    APP.use(checkAuthMiddleware);

    times["ACL & Auth"] = NI.timerInSeconds()-startTime;
  }

  //----------------------------------------------------------------------
  // Webpack devserver resources
  //----------------------------------------------------------------------
  if (appConfig.dirStaticBuild) {
    if (global.isProduction || global.isTestServer) {
      APP.use('/build', EXPRESS.static(global.ROOT_DIR + appConfig.dirStaticBuild));
    }
    else {
      startTime = NI.timerInSeconds();

      var proxyUrl = 'http://localhost:' + global.bundlePort;

      NI.log("Starting proxy on: " + proxyUrl);

      // Any requests to /build is proxied to webpack-dev-server.
      var httpProxy = require('http-proxy');
      var proxy = httpProxy.createProxyServer({
        changeOrigin: true
      });

      APP.all('/build/*', function (req, res) {
        proxy.web(req, res, { target: proxyUrl });
      });

      // It is important to catch any errors from the proxy or the server will
      // crash. An example of this is connecting to the server when webpack is
      // bundling.
      proxy.on('error', function(/*err*/) {
        NI.log("Could not connect to webpack dev server, please try again...");
      });

      times["WebPack Proxy"] = NI.timerInSeconds()-startTime;
    }
  }

  //----------------------------------------------------------------------
  // Static
  //----------------------------------------------------------------------
  {
    startTime = NI.timerInSeconds();
    APP.use('/', EXPRESS.static(global.ROOT_DIR + appConfig.dirStatic));
    times["Static"] = NI.timerInSeconds()-startTime;
  }

  //----------------------------------------------------------------------
  // Setup user defined routes
  //----------------------------------------------------------------------
  if (aOnSetupRoutes) {
    startTime = NI.timerInSeconds();
    NI.log("Server setting up routes...");
    aOnSetupRoutes(this);
    times["Setup user Routes"] = NI.timerInSeconds()-startTime;
  }

  //----------------------------------------------------------------------
  // Log routes
  //----------------------------------------------------------------------
  if (appConfig.logsRoute) {
    GET_LOGS(appConfig.logsRoute, appConfig.logsDir);
  }

  //----------------------------------------------------------------------
  // 404
  //----------------------------------------------------------------------
  APP.get('*', function(req, res) {
    SERVER_ERROR(req, res, undefined, { status: 404 });
  });
  APP.use(function(err, req, res, next) { // eslint-disable-line
    if (err.status) {
      res.statusCode = err.status;
    }
    SERVER_ERROR(req, res, err);
  });

  //----------------------------------------------------------------------
  // Start Server
  //----------------------------------------------------------------------
  startTime = NI.timerInSeconds();
  SERVER.listen(global.serverPort);
  NI.log("'%s' server started on %s:%s", global.APP_NAME, global.serverIP, global.serverPort);
  NI.log("network addresses: %K", getNetworkAddresses());
  times["Server Listen"] = NI.timerInSeconds()-startTime;

  //----------------------------------------------------------------------
  // Dump data for testing
  //----------------------------------------------------------------------
  if (!global.isProduction) {
    startTime = NI.timerInSeconds();

    FS.mkdirSync(global.ROOT_DIR + "/tests/");

    var testDataPath = global.ROOT_DIR + '/tests/_test_data';
    var serverType = appConfig.serverType;

    if(NI.isNotEmpty(serverType)) {
      testDataPath += '_' + serverType + '.json';
    } else {
      testDataPath += '.json';
    }

    NI.log("Dumping data for testing to: " + testDataPath);
    var data = {
      routes_page: global._routes_page,
      routes_get: global._routes_get,
      routes_post: global._routes_post,
    };
    NI.jsonWriteFileSync(testDataPath,data,{ spaces: " " });

    times["Dump Test Data"] = NI.timerInSeconds()-startTime;
  }

  NI.log("Server started in %.3fs: %K", (NI.timerInSeconds() - startTime0), times);
  NI.log("---- SERVER STARTED ---------------------------------------------");
  NI.log("URL: " + getThisUrl());
  return this;
}
exports.startServer = startServer;

function getThisUrl(aAfter) {
  var url = (appConfig.redirectAllToHttps ? "https://" : "http://") + appConfig.cookieDomain + ":" + global.serverPort;
  if (aAfter) {
    if (!NI.stringStartsWith(aAfter,"/")) {
      url += "/";
    }
    url += aAfter;
  }
  return url;
}
exports.getThisUrl = getThisUrl;

function startREPL(aContext) {
  var repl = require("repl");
  var replServer = repl.start({
    prompt: "route> ",
  });
  replServer.context.NI = NI;
  replServer.context.global = global;
  replServer.context.MOMENT = require('moment');
  NI.forEach(aContext,function(val,key) {
    replServer.context[key] = val;
  })
}
exports.startREPL = startREPL;

function stopServer() {
  SERVER.close();
}
exports.stopServer = stopServer;
