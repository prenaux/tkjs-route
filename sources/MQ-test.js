/* global FIXTURE, TEST, TEST_ASYNC, SHUTDOWN */
var NI = require('tkjs-ni');
var MQ = require('./MQ');

var TestMessages = {
  ping: {},
  pong: {},
};

FIXTURE("MQ-local", function() {
  var numPong = 0;
  var expectedNumPong = 0;

  TEST("send/receive1 test that its not synchronous", function(done) {
    var TestMQ = MQ.create("TestMQ",TestMessages);
    var startPong = numPong;
    var localPong = 0;
    // receive ping, send pong
    TestMQ.receiveMessage("ping", function() {
      TestMQ.sendMessage("pong");
    });
    TestMQ.receiveMessage("pong", function() {
      ++numPong;
      NI.print("... pong1: %d (%d)", numPong, localPong);
      if (++localPong == 3) {
        if (done) {
          done();
        }
      }
      else {
        TestMQ.sendMessage("ping");
      }
    });
    TestMQ.sendMessage("ping");
    // this should do nothing...
    NI.assert.equals(startPong, numPong);
    NI.assert.isUndefined(done);
    expectedNumPong += 3; // async, so shutdown should still expect 3 more pongs
  });

  TEST_ASYNC("send/receive2 async", function(done) {
    var TestMQ = MQ.create("TestMQ",TestMessages);
    var startPong = numPong;
    var localPong = 0;
    // receive ping, send pong
    TestMQ.receiveMessage("ping", function() {
      TestMQ.sendMessage("pong");
    });
    TestMQ.receiveMessage("pong", function() {
      ++numPong;
      NI.print("... pong2: %d (%d)", numPong, localPong);
      if (++localPong == 3) {
        done();
      }
      else {
        TestMQ.sendMessage("ping");
      }
    });
    TestMQ.sendMessage("ping");
    NI.assert.equals(startPong, numPong);
    NI.assert.isFunction(done);
    expectedNumPong += 3;
  });

  SHUTDOWN(function() {
    NI.assert.equal(expectedNumPong, numPong);
  });
});
