/* global __DEV__ */
var NI = require('tkjs-ni');
var MODEL = require("tkjs-model");

var MessageQueue = {
  _logMessages: __DEV__,
  _name: "unnamed",
  _messages: {},

  sendMessage: function(aMsgId, aMsgContent) {
    var that = this;
    var payload = that.createPayload(aMsgId, aMsgContent);
    if (that._logMessages === 2) {
      NI.log(
        "MQ '%s': sendMessage: payload: %K.",
        that._name, payload);
    }
    else if (that._logMessages) {
      NI.log(
        "MQ '%s': sendMessage: i: %K.",
        that._name, payload.i);
    }
    that.dispatchPayload(payload);
  },

  createPayload: function(aMsgId, aMsgContent) {
    var that = this;
    NI.assert.has(that._messages, aMsgId);
    var messageDesc = that._messages[aMsgId];
    var m = aMsgContent;
    if (messageDesc) {
      if (messageDesc.model) {
        m = MODEL.assertValidate(m, messageDesc.model);
      }
      if (messageDesc.validate) {
        m = messageDesc.validate(m);
      }
    }
    var payload = {
      q: that._name,
      i: aMsgId,
      m: m
    }
    return payload;
  },

  dispatchPayload: function(aPayload) {
    var that = this;
    if (!aPayload.i) {
      NI.warn("Payload doesn't have a message id 'i' field.");
      return;
    }
    var messageHandlers = that._messages[aPayload.i].handlers;
    if (!messageHandlers.length) {
      NI.warn("MessageQueue '"+ that._name +"': Received message '" +
              aPayload.i + "' but no one is handling it.");
    }
    else {
      for (var i = messageHandlers.length-1; i >= 0; --i) {
        that.dispatchPayloadToSingleHandler(aPayload,messageHandlers[i]);
      }
    }
  },

  dispatchPayloadToSingleHandler: function(aPayload,aHandler) {
    var that = this;
    if (typeof aHandler === "function") {
      NI.callNextTick(function() {
        aHandler(aPayload.m,that,aPayload.i);
      });
    }
    else {
      NI.httpRequest({
        get: aHandler.get,
        post: aHandler.post,
        jsonData: aPayload.m,
        onReply: function(aReplyData) {
          if (aHandler.reply) {
            that.sendMessage(aHandler.reply, aReplyData.OK);
          }
          if (aHandler.onReply) {
            aHandler.onReply(aReplyData);
          }
        },
        onError: function(aFetchError) {
          if (aHandler.error && aHandler.error.length > 0) {
            that.sendMessage(aHandler.error, aFetchError);
          }
          if (aHandler.onError) {
            aHandler.onError(aFetchError);
          }
        }
      });
    }
  },

  validateHandler: function(aMsgId,aHandler) {
    if (typeof aHandler === "function") {
      return true;
    }
    else if (typeof aHandler === "object") {
      NI.forEach(aHandler, function(val,key) {
        if (!(key === "get" || key === "post" ||
              key === "model" ||
              key === "handlers" ||
              key === "reply" || key === "onReply" ||
              key === "error" || key === "onError"))
        {
          throw NI.Error("Message '"+aMsgId+"', invalid handler object, invalid key '%s'", key);
        }
      });
      NI.invariant(
        !(("post" in aHandler) && ("get" in aHandler)),
        "Message '"+aMsgId+"', invalid handler object, both post and get urls have been specified." + NI.toDebugString(aHandler));
      NI.invariant(
        (("post" in aHandler) || ("get" in aHandler)),
        "Message '"+aMsgId+"', invalid handler object, no post or get urls has been specified.");
      NI.invariant(
        (("reply" in aHandler) || ("onReply" in aHandler)),
        "Message '"+aMsgId+"', invalid handler object, no reply handler specified.");
      NI.invariant(
        (("error" in aHandler) || ("onError" in aHandler)),
        "Message '"+aMsgId+"', invalid handler object, no error handler specified.");
      NI.invariant(
        (!("model" in aHandler) ||
         MODEL.hasModel(aHandler.model)),
        "Message '"+aMsgId+"', invalid handler object, no model '%s' found.",
        aHandler.model);

      return true;
    }
    throw NI.Error("Invalid type of handler: " + (typeof aHandler));
  },

  receiveMessage: function(aMsgId, aHandler) {
    var that = this;
    NI.assert.has(that._messages, aMsgId);
    that.validateHandler(aMsgId,aHandler);
    var messageHandlers = that._messages[aMsgId].handlers;
    messageHandlers.push(aHandler);

  },
};

function createMessageQueue(aName,aMessages) {
  NI.assert.isObject(aMessages);
  NI.assert.isString(aName);

  // Create a new message queue of this type with only the default handlers
  // registered.
  function newMessageQueue() {
    var mq = NI.clone(MessageQueue);
    mq._name = aName;

    NI.forEach(aMessages,function(aMsgDesc,aMsgName) {
      NI.assert.isString(aMsgName);
      NI.assert.hasnt(mq._messages,aMsgName);
      var msgDesc = aMsgDesc ? NI.clone(aMsgDesc) : {};

      if ((typeof msgDesc === "function") || msgDesc.get || msgDesc.post) {
        msgDesc.handlers = [msgDesc];
      }
      else if (!msgDesc.handlers) {
        msgDesc.handlers = [];
      }

      NI.forEach(msgDesc.handlers,function(aHandler) {
        mq.validateHandler(aMsgName,aHandler);
      });

      mq._messages[aMsgName] = msgDesc;
    });

    mq.new = newMessageQueue;
    return mq;
  };

  return newMessageQueue();
}
exports.create = createMessageQueue;
